---
title: NetHunter Home Screen
description:
icon:
date: 2019-11-29
type: post
weight: 190
author: ["re4son",]
tags: ["",]
keywords: ["",]
og_description:
---

The NetHunter Home screen provides a common place to see some useful, frequently-used information about your device, including both external and internal IP addresses, as well as the availability of your HID interfaces.

![](./nethunter-home.png)
